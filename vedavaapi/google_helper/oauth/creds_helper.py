# -*- coding: utf-8 -*-

import json

import google.oauth2.credentials
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from google.auth.transport.requests import Request



def credentials_from_file(creds_file, scopes, auth_through_service_account=False):
    creds = None;

    try:
        if auth_through_service_account:
            creds = service_account.Credentials.from_service_account_info(json.loads(open(creds_file, 'rb').read().decode('utf-8')), scopes=scopes)
        else:
            print(creds_file)
#            creds = google.oauth2.credentials.Credentials(**json.loads(open(creds_file, 'rb').read().decode('utf-8'))) #json.load() producing UnicodeDecodeError in production wsgi env some times.

            creds = Credentials.from_authorized_user_file(creds_file, scopes)

            if not creds or not creds.valid:
                try:
                    if creds and creds.expired and creds.refresh_token:
                        creds.refresh(Request())
                    else:
                        raise IOError('Missing credentials. Need to re-authenticate')
                except Exception as e:
                    raise IOError('Expired credentials. Need to re-authenticate', e)
    except Exception as err:
        raise IOError('error in loading credentials file from given path', err);


    return creds


def credentials_from_string(creds_string, scopes, auth_through_service_account=False):
    credentials = None;

    if auth_through_service_account:
        credentials = service_account.Credentials.from_service_account_info(json.loads(creds_string, encoding='utf-8'),scopes=scopes)
    else:
        credentials = google.oauth2.credentials.Credentials(**json.loads(creds_string, encoding='utf-8'))

    return credentials


def credentials_from_dict(creds_dict, scopes, auth_through_service_account=False):
    credentials = None;

    if auth_through_service_account:
        credentials = service_account.Credentials.from_service_account_info(creds_dict, scopes=scopes)
    else:
        credentials = google.oauth2.credentials.Credentials(**creds_dict)
    return credentials


def credentials_to_dict(credentials):
    cdict = {'token': credentials.token,
                'refresh_token': credentials.refresh_token,
                'token_uri': credentials.token_uri,
                'client_id': credentials.client_id,
                'client_secret': credentials.client_secret,
                'scopes': credentials.scopes}
    return cdict




